package Controllers

import (
	"fmt"
	"log"
	"net/http"
	"text/template"
)

func GetMatrixPage(w http.ResponseWriter, r *http.Request) {

	files := []string{
		"./Templates/base.html",
		"./Templates/header.html",
		"./Templates/footer.html",
		"./Templates/matrix.html",
	}

	tpl, err := template.ParseFiles(files...)

	if err != nil {
		log.Fatal(err)
	}

	err = tpl.ExecuteTemplate(w, "base", nil)

	if err != nil {
		log.Fatal(err)
	}
}

func CalculateMatrix(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Response json fields:")
}
