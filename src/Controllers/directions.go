package Controllers

import (
	"fmt"
	"log"
	"net/http"
	"text/template"
)

func GetDirectionPage(w http.ResponseWriter, r *http.Request) {

	files := []string{
		"./Templates/base.html",
		"./Templates/header.html",
		"./Templates/footer.html",
		"./Templates/direction.html",
	}

	tpl, err := template.ParseFiles(files...)

	if err != nil {
		log.Fatal(err)
	}

	err = tpl.ExecuteTemplate(w, "base", nil)

	if err != nil {
		log.Fatal(err)
	}
}

func CalculateRoute(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Response json fields:")
}
