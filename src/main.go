package main

import (
	"MapsService/Routers"
	"fmt"
	"log"
	"net/http"
)

func main() {
	fmt.Println("Starting Server ...")

	//Auto Migration

	//Init Router
	r := Routers.InitRouter()

	//Start Server
	log.Fatal(http.ListenAndServe(":8080", r))

}
