package Routers

import (
	"MapsService/Controllers"
	"net/http"

	"github.com/gorilla/mux"
)

func InitRouter() *mux.Router {

	r := mux.NewRouter().StrictSlash(true)

	r.HandleFunc("/", Controllers.GetIndexPage).Methods("GET")

	r.HandleFunc("/direction", Controllers.GetDirectionPage).Methods("GET")
	r.HandleFunc("/matrix", Controllers.GetMatrixPage).Methods("GET")

	r.HandleFunc("/direction", Controllers.CalculateRoute).Methods("POST")
	r.HandleFunc("/matrix", Controllers.CalculateMatrix).Methods("POST")

	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./Public/")))

	return r
}
